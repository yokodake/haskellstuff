#!/usr/bin/env stack
-- stack --resolver lts-11.15 --install-ghc runghc --package transformers

--{-# LANGUAGE TypeSynonymInstances #-}
--{-# LANGUAGE FlexibleInstances #-}

module State where

import System.Random
import Control.Applicative (liftA2)
import Control.Monad (replicateM)
import Control.Monad.Trans.State
import Control.Monad.Trans.State

randomR' (m, n) = randomR (m, n) :: StdGen -> (Int, StdGen)

rollDie :: State StdGen Int
rollDie = state $ randomR (1, 6)

rollDie' :: State StdGen Int
rollDie' = do generator <- get
              let (value, newGenerator) = randomR (1,6) generator
              put newGenerator
              return value

rollDice = liftA2 (,) rollDie rollDie
  -- liftA2 f x y = (f <$> x) <*> y

-- implement a function rollNDice :: Int -> State StdGen [Int] that,
--   given an integer, returns a list with that number of pseudo-random
--   integers between 1 and 6
rollNDice :: Int -> State StdGen [Int]
rollNDice n = replicateM n rollDie

-- Write an instance of Functor for State s.
--   No use of Monad allowed
newtype MST s a = MST (State s a)

instance Functor (MST s) where
  fmap f (MST st) = MST . state $
                    \s1 -> case runState st s1 of
                      (a, s2) -> (f a, s2)

-- implement modify and gets
modify :: (s -> s) -> State s ()
modify f = state $ \s -> ((), f s)

gets :: (s -> a) -> State s a
gets f = do s <- get
            return (f s)

main' = do let (MST foo) = (+1) <$> (MST rollDie)
           print $ evalState (replicateM 4 foo) (mkStdGen 0)

adder :: Int -> (Int -> Int, Int)
adder s = (\x -> x+s, s+1)

stateAdder :: State Int (Int -> Int)
stateAdder = state adder
