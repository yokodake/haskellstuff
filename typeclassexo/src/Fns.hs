module Fns where

import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Function

newtype Func a b = Func {fn :: (a -> b)}

instance Functor (Func a)  where
  fmap f (Func fn) = Func (f . fn)

instance Applicative (Func a) where
  pure b = Func (const b)
  (Func atob) <*> (Func toa) = Func $ \x -> (atob x) (toa x)

instance Monad (Func a) where
  return = pure
  (Func f) >>= m = Func $ \x -> fn ((m . f) x) x

tests = hspec $ do
  describe "Functor" $ do
    it "identity" $
      property (fid :: Fun Int String -> Int -> Bool)
    it "composition" $
      property (fcomp :: FtorComp Float String Bool Int)
  describe "Applicative" $ do
    it "identity" $
      property (aid :: Fun Int String -> Int -> Bool)
    it "composition" $
      property (acomp :: AppComp Float String Bool Int)
    it "homomorphism" $
      property (ahomo :: AppHomo Float String Int)
    it "interchange" $
      property (aint :: AppInter Float Bool Int)
  describe "Monad" $ do
    it "left Id" $
      property (midl :: Monid String Int)
    it "Right Id" $
      property (midr :: Fun Int String -> Int -> Bool)
    it "Associativity" $
      property (massoc :: MonAssoc Float String Bool Int)



type FtorComp a b c f = Fun b c -> Fun a b -> Fun f a -> f -> Bool
type AppComp a b c f = Fun f (Fun b c) -> Fun f (Fun a b) -> Fun f a -> f -> Bool
type AppHomo a b f = Fun a b -> a -> f -> Bool
type AppInter a b f = Fun f (Fun a b) -> a -> f -> Bool

type Monid a f = Fun f (Fun f a) -> f -> f -> Bool
type MonAssoc a b c f = Fun f a -> Fun a (Fun f b) -> Fun b (Fun f c) -> f -> Bool

exe fun x = fn fun x
nestedFun = (\x -> apply . apply x)

fid fun a = ((id <$> func) `exe` a) == func `exe` a
  where
    func = Func (apply fun)
fcomp f g fun a = ((fmap f' . fmap g') func `exe` a) == (fmap (f' . g') func `exe` a)
  where
    func = Func (apply fun)
    f' = apply f
    g' = apply g

aid fun a = ((pure id <*> v) `exe` a) == v `exe` a
  where v = Func (apply fun)
acomp f g h a = ((pure (.) <*> u <*> v <*> w) `exe` a) == ((u <*> (v <*> w)) `exe` a)
  where
    u = Func $ nestedFun f
    v = Func $ nestedFun g
    w = Func $ apply h
ahomo fun x a = ((pure f <*> pure x) `exe` a) == (pure (f x) `exe` a)
  where f = apply fun
aint f y a = (u <*> pure y) `exe` a == (pure ($y) <*> u) `exe` a
  where
    u = Func $ nestedFun f

midl f x a = (return x >>= k) `exe` a == k x `exe` a
  where k = (Func . apply) <$> apply f
midr f a = (m >>= return) `exe` a   == m `exe` a
  where m = Func $ apply f
massoc f g h a = (m >>= (\x -> k x >>= n)) `exe` a == ((m >>= k) >>= n) `exe` a
  where
    m = Func $ apply f
    k = (Func . apply) <$> apply g
    n = (Func . apply) <$> apply h
