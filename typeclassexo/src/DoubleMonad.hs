module DoubleMonad where

import Data.Monoid ((<>))

newtype D a b = D { d :: (a, b) } deriving (Eq, Show, Read, Ord)

instance Functor (D a) where
  fmap f = D . fmap f . d

instance Monoid a => Applicative (D a) where
  pure = D . pure
  (D (u, f)) <*> (D (v, x)) = D (u <> v, f x)


instance Monoid a => Monad (D a) where
  return = pure
  (D (u, a)) >>= k = case k a of
                       (D (v, b)) -> D (u <> v, b)
