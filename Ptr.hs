module Main where

import Foreign.Ptr (Ptr(..), castPtr)
import Foreign.Marshal.Alloc(mallocBytes, free, malloc)
import Foreign.Marshal.Array(peekArray)
import Foreign.Storable (Storable(..), pokeElemOff, poke)
import Data.Word (Word8(..))
import Data.Int (Int8(..), Int32(..))


size = 4
main :: IO ()
main = do p <- malloc
          poke p (0x04030201 :: Int32)
          peekArray size (castPtr p :: Ptr Int8) >>= print
          free p

fillBuffer :: Ptr Word8 -> Int -> IO ()
fillBuffer ptr size = go 0
  where
    go i | i == size = return ()
         | otherwise = do pokeElemOff ptr i (fromIntegral $ 0x41+i)
                          go (succ i)
