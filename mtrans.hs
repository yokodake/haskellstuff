module Mtrans where

import Control.Applicative (Alternative(..))
import Control.Monad (MonadPlus(..), liftM, guard)
import Control.Monad.Trans.Class (MonadTrans(..))

newtype MaybeT m a = MaybeT { runMaybeT :: m (Maybe a) }

instance Monad m => Functor (MaybeT m) where
  fmap f x = MaybeT $ (fmap . fmap) f (runMaybeT x)

instance Monad m => Applicative (MaybeT m) where
  pure = return
  f <*> x = MaybeT $ do mf <- runMaybeT f
                        (mf <*>) <$> (runMaybeT x)

instance Monad m => Alternative (MaybeT m) where
  empty = MaybeT $ return Nothing
  x <|> y = MaybeT $ do maybe_value <- runMaybeT x
                        case maybe_value of
                          Nothing -> runMaybeT y
                          Just _  -> return maybe_value

instance Monad m => Monad (MaybeT m) where
  return = MaybeT . return . Just
  x >>= f = MaybeT $ do maybe_value <- runMaybeT x
                        case maybe_value of
                          Nothing -> return Nothing
                          Just value -> runMaybeT $ f value

instance Monad m => MonadPlus (MaybeT m) where
  mzero = empty
  mplus = (<|>)

instance MonadTrans MaybeT where
  lift = MaybeT . (liftM Just)


isValid :: String -> Bool
isValid x = undefined
-- rewrite getPassphrase
getPassphrase :: MaybeT IO String
getPassphrase = do s <- lift getLine
                   guard (isValid s)
                   return s
askPassphrase :: MaybeT IO ()
askPassphrase = do lift $ putStrLn "Insert your new passphrase:"
                   value <- getPassphrase
                   lift $ putStrLn "Storing in database..."
