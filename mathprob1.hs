{-# LANGUAGE LambdaCase #-}

module Main where

main :: IO ()
main = return ()

data Dir = L | R deriving (Show, Eq)
data P = P { spd ::Double,
             pos ::Double,
             idc ::Char } deriving (Show)


data Map a b = Map a b
