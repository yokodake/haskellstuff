#!/usr/bin/env stack
-- stack --resolver lts-11.15 --install-ghc runghc --package QuickCheck --package transformers

import Control.Applicative (Alternative(..), liftA2)
import Control.Monad (guard)
import Control.Monad.Trans.State (State(..))
import Data.Char (ord)
import Data.Foldable (asum)
import Test.QuickCheck

main = undefined

char :: Char -> String -> Maybe (Char, String)
char _ [] = Nothing
char c s = do
  let (c':s') = s
  if c == c'
    then Just (c, s')
    else Nothing

digit :: Int -> String -> Maybe (Int, String)
digit _ [] = Nothing
digit i s | i > 9 || i < 0 = Nothing
          | otherwise      = do let (c:s') = s
                                if [c] == show i
                                  then Just (i, s')
                                  else Nothing

pdigit str = asum $ map ($str) fs
  where
    fs = map digit [1..9]
pchar str = (ctoh<$>) . asum $ map ($str) fs
  where
    fs = map char ['a'..'f']

phex = liftA2 (<|>) pdigit pchar

ctoh (c, x) = (c', x)
  where
    c' = (ord c) - 87


-- Use guard and Applicative combinators to implement safeLog
safeLog :: (Floating a, Ord a) => a -> Maybe a
safeLog x = guard (x > 0) *> Just (log x)
